module main.mongodb {
  requires javafx.controls;
  requires javafx.fxml;
  requires org.mongodb.driver.core;
  requires org.mongodb.driver.sync.client;
  requires org.mongodb.bson;

  opens main.mongodb to javafx.fxml;
  exports main.mongodb;
  exports main.mongodb.controllers;
  opens main.mongodb.controllers to javafx.fxml;
}