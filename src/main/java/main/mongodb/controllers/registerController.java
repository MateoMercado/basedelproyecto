package main.mongodb.controllers;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import main.mongodb.controllers.users.Register;

public class registerController {
  @FXML
  private TextField userName;

  @FXML
  private TextField correoField;

  @FXML
  private TextField nameField;

  @FXML
  private TextField lastName;

  @FXML
  private PasswordField contrasenaField;

  @FXML
  private PasswordField confirmarContrasenaField;

  @FXML
  public void backRegisterButtonOnAction(ActionEvent event) throws IOException {
    windowsRedirect.goToLogin(event);
  }
  @FXML
  public void registerButtonOnAction(ActionEvent event) throws IOException {
    boolean ingresaa = true;
    String user = userName.getText();
    String gmail = correoField.getText();
    String name = nameField.getText();
    String last = lastName.getText();
    String password = contrasenaField.getText();
    String confirmPassword = confirmarContrasenaField.getText();

    if (name.isEmpty() || gmail.isEmpty() || last.isEmpty() || user.isEmpty() ||password.isEmpty()
        || confirmPassword.isEmpty()) {
      userName.getStyleClass().add("error-textfield");
      correoField.getStyleClass().add("error-textfield");
      nameField.getStyleClass().add("error-textfield");
      lastName.getStyleClass().add("error-textfield");
      contrasenaField.getStyleClass().add("error-textfield");
      confirmarContrasenaField.getStyleClass().add("error-textfield");
      ingresaa = false;
      return;
    }

    String emailRegex = "^[A-Za-z0-9+_.-]+@(gmail\\.com|jala\\.university)$";
    if (!gmail.matches(emailRegex)) {
      correoField.getStyleClass().add("error-textfield");
      ingresaa = false;
      return;

    }

    if (!password.equals(confirmPassword)) {
      contrasenaField.getStyleClass().add("error-textfield");
      confirmarContrasenaField.getStyleClass().add("error-textfield");
      ingresaa = false;
      return;

    }

    if (ingresaa) {
      Register register = new Register();
      register.registerUser(user,password,name,last,gmail);

      showAlert("Éxito", "Registro exitoso");
      windowsRedirect.goToLogin(event);
    }
  }
  private void showAlert(String title, String message) {
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setTitle(title);
    alert.setHeaderText(null);
    alert.setContentText(message);
    alert.showAndWait();
  }

}
