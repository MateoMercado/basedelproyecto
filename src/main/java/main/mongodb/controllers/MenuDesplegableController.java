package main.mongodb.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class MenuDesplegableController {

  @FXML
  private TextField textField;

  public String obtenerTextoIngresado() {
    return textField.getText();
  }
}
