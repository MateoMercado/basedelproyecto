package main.mongodb.controllers.users;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class MongoRegister {

  private static final String CONNECTION_STRING = "mongodb+srv://mateoPrueba:Admin123@pruebadbproject.nmcrt2q.mongodb.net/";
  private static final String DATABASE_NAME = "ToDoList";
  private static final String COLLECTION_NAME = "users";

  private MongoClient mongoClient;
  private MongoDatabase database;
  private MongoCollection<Document> collection;

  public MongoRegister() {
    mongoClient = MongoClients.create(CONNECTION_STRING);
    database = mongoClient.getDatabase(DATABASE_NAME);
    collection = database.getCollection(COLLECTION_NAME);
  }

  public void insertUser(Document user) {
    collection.insertOne(user);
  }

  public void close() {
    mongoClient.close();
  }
}
