package main.mongodb.controllers.users;

import org.bson.Document;

public class Login {

  private final MongoUser dbManager;

  public Login() {
    dbManager = new MongoUser();
  }

  public boolean authenticate(String username, String password) {
    Document user = dbManager.getUserData(username);

    if (user != null) {
      String storedPassword = user.getString("password");
      return password.equals(storedPassword);
    }

    return false;
  }

}
