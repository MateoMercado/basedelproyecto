package main.mongodb.controllers.users;

import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import org.bson.Document;

public class MongoUser {

  private static final String CONNECTION_STRING = "mongodb+srv://mateoPrueba:Admin123@pruebadbproject.nmcrt2q.mongodb.net/";
  private static final String DATABASE_NAME = "ToDoList";
  private static final String COLLECTION_NAME = "users";

  private MongoClient mongoClient;
  private MongoDatabase database;
  private MongoCollection<Document> collection;

  public MongoUser() {
    mongoClient = MongoClients.create(CONNECTION_STRING);
    database = mongoClient.getDatabase(DATABASE_NAME);
    collection = database.getCollection(COLLECTION_NAME);
  }

  public Document getUserData(String username) {
    return collection.find(Filters.eq("username", username)).first();
  }

  public void close() {
    mongoClient.close();
  }
}