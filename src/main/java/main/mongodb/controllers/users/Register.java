package main.mongodb.controllers.users;

import org.bson.Document;

public class Register {

  private MongoRegister dbManager;

  public Register() {
    dbManager = new MongoRegister();
  }

  public void registerUser(String username, String password, String nombre, String apellido, String correo) {
    if (!correo.endsWith("@gmail.com")) {
      System.out.println("El correo debe ser de dominio @gmail.com");
      return;
    }

    Document user = new Document("username", username)
        .append("password", password)
        .append("nombre", nombre)
        .append("apellido", apellido)
        .append("correo", correo);

    dbManager.insertUser(user);
  }

}
