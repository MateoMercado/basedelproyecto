package main.mongodb.controllers;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class dayCotroller {

  @FXML
  private Button idBtnUser;
  @FXML
  public void myDay(ActionEvent event) throws IOException {
    windowsRedirect.goToMyDay(event);
  }
  @FXML
  public void important(ActionEvent event) throws IOException {
    windowsRedirect.goToImportant(event);
  }
  @FXML
  public void planned(ActionEvent event) throws IOException {
    windowsRedirect.goToPlanned(event);
  }
  @FXML
  public void assigned(ActionEvent event) throws IOException {
    windowsRedirect.goToAsigment(event);
  }
  @FXML
  public void flaged(ActionEvent event) throws IOException {
    windowsRedirect.goToFlaged(event);
  }
  @FXML
  public void task(ActionEvent event) throws IOException {
    windowsRedirect.goToTask(event);
  }
  @FXML
  private void abrirTask() {
    try {
      FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/mongodb/textField.fxml"));
      Parent root = loader.load();

      MenuDesplegableController controller = loader.getController();

      Stage stage = new Stage();
      stage.initModality(Modality.APPLICATION_MODAL);
      stage.setTitle("Menú Desplegable");
      stage.setScene(new Scene(root));

      stage.showAndWait();

      String textoIngresado = controller.obtenerTextoIngresado();
      System.out.println("Texto ingresado: " + textoIngresado);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
