package main.mongodb.controllers;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import main.mongodb.controllers.users.Login;

public class loginControlers {
  @FXML
  private TextField usernameTextField;

  @FXML
  private PasswordField passwordTextField;

  @FXML
  public void loginButtonOnAction(ActionEvent event) throws IOException {
    String username = usernameTextField.getText();
    String password = passwordTextField.getText();
    Login login = new Login();

    boolean verificationAccess = login.authenticate(username, password);
    System.out.println("verificationAccess: " + verificationAccess);
    if (verificationAccess) {
      redireccionar(event);
    } else {
      usernameTextField.getStyleClass().add("error-textfield");
      passwordTextField.getStyleClass().add("error-textfield");
    }
  }
  @FXML
  public void registerButtonOnAction(ActionEvent event) throws IOException {
    windowsRedirect.goToRegister(event);
  }
  @FXML
  public void redireccionar(ActionEvent event) throws IOException {
    windowsRedirect.goToMyDay(event);
  }
}
