package main.mongodb.controllers;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class windowsRedirect {
  public static void redirectToPage(ActionEvent event, String fxmlPath, int width, int height) {
    try {
      FXMLLoader loader = new FXMLLoader(windowsRedirect.class.getResource(fxmlPath));
      Parent root = loader.load();
      Scene scene = new Scene(root, width, height);

      Stage currentStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
      currentStage.setScene(scene);
      currentStage.show();

      System.out.println("Redirecting to page: " + fxmlPath);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }


  public static void goToWellcome(ActionEvent event) throws IOException {
    redirectToPage(event, "/main/mongodb/wellcome.fxml", 1350, 700);
  }

  public static void goToMyDay(ActionEvent event) throws IOException {
    redirectToPage(event, "/main/mongodb/myDay.fxml", 1350, 700);
  }

  public static void goToImportant(ActionEvent event) throws IOException {
    redirectToPage(event, "/main/mongodb/important.fxml", 1350, 700);
  }

  public static void goToPlanned(ActionEvent event) throws IOException {
    redirectToPage(event, "/main/mongodb/planned.fxml", 1350, 700);
  }

  public static void goToAsigment(ActionEvent event) throws IOException {
    redirectToPage(event, "/main/mongodb/asigment.fxml", 1350, 700);
  }

  public static void goToFlaged(ActionEvent event) throws IOException {
    redirectToPage(event, "/main/mongodb/flaged.fxml", 1350, 700);
  }

  public static void goToTask(ActionEvent event) throws IOException {
    redirectToPage(event, "/main/mongodb/task.fxml", 1350, 700);
  }

  public static void goToLogin(ActionEvent event) throws IOException {
    redirectToPage(event, "/main/mongodb/login.fxml", 1350, 700);
  }

  public static void goToRegister(ActionEvent event) throws IOException {
    redirectToPage(event, "/main/mongodb/register.fxml", 1350, 700);
  }

  public static void goToInformation(ActionEvent event) throws IOException {
    redirectToPage(event, "/main/mongodb/information.fxml", 1350, 700);
  }

}
