package main.mongodb.controllers;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class wellcomeController {
  @FXML
  public void information(ActionEvent event) throws IOException {
    windowsRedirect.goToInformation(event);
  }
  @FXML
  public void getStarted(ActionEvent event) throws IOException {
    windowsRedirect.goToLogin(event);
  }
  @FXML
  public void loginRegister(ActionEvent event) throws IOException {
    windowsRedirect.goToRegister(event);
  }

}
