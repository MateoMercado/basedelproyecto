package main.mongodb.controllers;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class taskController {
  @FXML
  public void myDay(ActionEvent event) throws IOException {
    windowsRedirect.goToMyDay(event);
  }
  @FXML
  public void important(ActionEvent event) throws IOException {
    windowsRedirect.goToImportant(event);
  }
  @FXML
  public void planned(ActionEvent event) throws IOException {
    windowsRedirect.goToPlanned(event);
  }
  @FXML
  public void assigned(ActionEvent event) throws IOException {
    windowsRedirect.goToAsigment(event);
  }
  @FXML
  public void flaged(ActionEvent event) throws IOException {
    windowsRedirect.goToFlaged(event);
  }
  @FXML
  public void task(ActionEvent event) throws IOException {
    windowsRedirect.goToTask(event);
  }

}
